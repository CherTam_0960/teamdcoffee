/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class Material {

    private int id;
    private String name;
    private double pricePerUnit;
    private String unit;
    private String date;

    public Material(int id, String name, double pricePerUnit, String unit, String date) {
        this.id = id;
        this.name = name;
        this.pricePerUnit = pricePerUnit;
        this.unit = unit;
        this.date = date;
    }

    public Material(String name, double pricePerUnit, String unit, String date) {
        this.id = -1;
        this.name = name;
        this.pricePerUnit = pricePerUnit;
        this.unit = unit;
        this.date = date;
    }

    public Material() {
        this.id = -1;
        this.name = name;
        this.pricePerUnit = pricePerUnit;
        this.unit = unit;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", pricePerUnit=" + pricePerUnit + ", unit=" + unit + ", date=" + date + '}';
    }

    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("material_id"));
            material.setName(rs.getString("material_name"));
            material.setPricePerUnit(rs.getDouble("material_price_per_unit"));
            material.setUnit(rs.getString("material_unit"));
            material.setDate(rs.getString("material_estimate_date"));
        } catch (SQLException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }
}
