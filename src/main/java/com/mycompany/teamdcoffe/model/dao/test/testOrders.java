/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao.test;

import com.mycompany.teamdcoffe.model.Orders;
import com.mycompany.teamdcoffe.model.dao.OrdersDao;

/**
 *
 * @author Nippon
 */
public class testOrders {

    public static void main(String[] args) {
        OrdersDao ordersDao = new OrdersDao();

        //Test Run Get Id
//        System.out.println(ordersDao.get(1));
        //Test Run GetAll 
//        System.out.println(ordersDao.getAll());
        //Test Run Save
//        Orders orders = new Orders(1, 3, 3, 9999, 9999, 9999, 1, 9999, 9999, "test");
//        ordersDao.save(orders);
//        System.out.println(ordersDao.getAll());
//        Test Run Update
        Orders orders = ordersDao.get(4);
        orders.setSell_type("store");
        ordersDao.update(orders);
        System.out.println(ordersDao.get(4));
        //    Test Run Delete
//        Orders orders = ordersDao.get(6);
//        ordersDao.delete(orders);
//        System.out.println(ordersDao.getAll());
        //Test Run getAll order by
//        ordersDao.getAll("order_cash", "order_cash ASC");
//        System.out.println(ordersDao.getAll("order_cash", "order_cash ASC"));
    }
}
