/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao;

import com.mycompany.teamdcoffe.helper.DatabaseHelper;
import com.mycompany.teamdcoffe.model.StockReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kitti
 */
public class StockReportDao implements Dao<StockReport> {

    public List<StockReport> getAll(String where, String order) {
        ArrayList<StockReport> list = new ArrayList();
        String sql = "SELECT stock.stock_id, stock.employee_id, employee.employee_name, material_detail.stock_check_id, material_detail.material_detail_minimum,material_detail.material_detail_remain FROM stock LEFT JOIN employee ON stock.employee_id = employee.employee_id LEFT JOIN material_detail ON stock.stock_id = material_detail.stock_check_id ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockReport item = StockReport.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockReport get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<StockReport> getAll() {
        ArrayList<StockReport> list = new ArrayList();
        String sql = "SELECT stock.stock_id, stock.employee_id, employee.employee_name, material_detail.stock_check_id, material_detail.material_detail_minimum,material_detail.material_detail_remain FROM stock LEFT JOIN employee ON stock.employee_id = employee.employee_id LEFT JOIN material_detail ON stock.stock_id = material_detail.stock_check_id ORDER BY stock_id ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockReport item = StockReport.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockReport save(StockReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public StockReport update(StockReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(StockReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
