--
-- File generated with SQLiteStudio v3.2.1 on พฤ. ต.ค. 6 13:29:24 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    customer_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    customer_name        TEXT (50) NOT NULL,
    customer_phone       TEXT (12) NOT NULL,
    customer_count_times INTEGER,
    customer_discount    TEXT (2)  DEFAULT (10) 
                                   NOT NULL
);

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_phone,
                         customer_count_times,
                         customer_discount
                     )
                     VALUES (
                         1,
                         'กำเนิด โควิด',
                         '086-1451234',
                         9,
                         '10%'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_phone,
                         customer_count_times,
                         customer_discount
                     )
                     VALUES (
                         2,
                         'สมทรง โฮ่ฮิ้ว',
                         '087-3596331',
                         12,
                         '10%'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_phone,
                         customer_count_times,
                         customer_discount
                     )
                     VALUES (
                         3,
                         'สมหมาย หมายปอง',
                         '094-6652282',
                         5,
                         '10%'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_phone,
                         customer_count_times,
                         customer_discount
                     )
                     VALUES (
                         4,
                         'สมศักดิ์  มักดี',
                         '096-3214568',
                         10,
                         '10%'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_phone,
                         customer_count_times,
                         customer_discount
                     )
                     VALUES (
                         5,
                         'สมศรี  มักน้อย',
                         '061-7758991',
                         4,
                         '10%'
                     );


-- Table: employee
DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
    employee_id      INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    employee_name    TEXT (50) NOT NULL,
    employee_phone   TEXT (12) NOT NULL,
    employee_address TEXT (80) NOT NULL,
    employee_salary  DOUBLE    NOT NULL,
    employee_role    TEXT (20) NOT NULL
);

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_phone,
                         employee_address,
                         employee_salary,
                         employee_role
                     )
                     VALUES (
                         1,
                         'จุลดิศ แก้วรุ่งเรือง',
                         '091-549-5959',
                         '15/559 ต.แสนสุข อ.เมืองชลบุรี จ.ชลบุรี 20130',
                         '12,000',
                         'พนักงานเคาน์เตอร์'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_phone,
                         employee_address,
                         employee_salary,
                         employee_role
                     )
                     VALUES (
                         2,
                         'พีระยุทธ พรมนอก',
                         '064-899-8989',
                         '85/13 ต.บางละมุง อ.บางละมุง จ.ชลบุรี 20150',
                         '12,000',
                         'พนักงานชงชา'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_phone,
                         employee_address,
                         employee_salary,
                         employee_role
                     )
                     VALUES (
                         3,
                         'ธนทญา วรทญานันทน์',
                         '062-428-9433',
                         '36/16 ต.หน้าเมือง อ.เมือง จ.ฉะเชิงเทรา 24000',
                         '12,000',
                         'พนักงานชงชา'
                     );


-- Table: fill_stock
DROP TABLE IF EXISTS fill_stock;

CREATE TABLE fill_stock (
    fill_stock_id     INTEGER      PRIMARY KEY,
    employee_id       INTEGER      REFERENCES employee (employee_id),
    fill_stock_total  TEXT (10, 2) NOT NULL,
    fill_stock_cash   TEXT (10, 2) NOT NULL,
    fill_stock_change TEXT (10, 2) NOT NULL
);

INSERT INTO fill_stock (
                           fill_stock_id,
                           employee_id,
                           fill_stock_total,
                           fill_stock_cash,
                           fill_stock_change
                       )
                       VALUES (
                           1,
                           1,
                           '2890',
                           '3000',
                           '110'
                       );

INSERT INTO fill_stock (
                           fill_stock_id,
                           employee_id,
                           fill_stock_total,
                           fill_stock_cash,
                           fill_stock_change
                       )
                       VALUES (
                           2,
                           2,
                           '3110',
                           '3200',
                           '90'
                       );

INSERT INTO fill_stock (
                           fill_stock_id,
                           employee_id,
                           fill_stock_total,
                           fill_stock_cash,
                           fill_stock_change
                       )
                       VALUES (
                           3,
                           3,
                           '1550',
                           '1600',
                           '50'
                       );


-- Table: order_item
DROP TABLE IF EXISTS order_item;

CREATE TABLE order_item (
    order_item_id            INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    product_id               INTEGER REFERENCES product (product_id) 
                                     NOT NULL,
    order_id                 INTEGER REFERENCES orders
                                     NOT NULL,
    order_item_total_product INTEGER NOT NULL,
    order_item_total_price   DOUBLE  NOT NULL
);

INSERT INTO order_item (
                           order_item_id,
                           product_id,
                           order_id,
                           order_item_total_product,
                           order_item_total_price
                       )
                       VALUES (
                           1,
                           1,
                           1,
                           1,
                           40.0
                       );

INSERT INTO order_item (
                           order_item_id,
                           product_id,
                           order_id,
                           order_item_total_product,
                           order_item_total_price
                       )
                       VALUES (
                           2,
                           2,
                           2,
                           1,
                           50.0
                       );

INSERT INTO order_item (
                           order_item_id,
                           product_id,
                           order_id,
                           order_item_total_product,
                           order_item_total_price
                       )
                       VALUES (
                           3,
                           3,
                           3,
                           1,
                           60.0
                       );

INSERT INTO order_item (
                           order_item_id,
                           product_id,
                           order_id,
                           order_item_total_product,
                           order_item_total_price
                       )
                       VALUES (
                           4,
                           4,
                           4,
                           1,
                           40.0
                       );

INSERT INTO order_item (
                           order_item_id,
                           product_id,
                           order_id,
                           order_item_total_product,
                           order_item_total_price
                       )
                       VALUES (
                           5,
                           5,
                           5,
                           1,
                           40.0
                       );


-- Table: orders
DROP TABLE IF EXISTS orders;

CREATE TABLE orders (
    order_id        INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    customer_id     INTEGER   REFERENCES customer (customer_id),
    employee_id     INTEGER   REFERENCES employee (employee_id) 
                              NOT NULL,
    store_id        INTEGER   REFERENCES store (store_id) 
                              NOT NULL,
    order_price     DOUBLE,
    order_discount  DOUBLE    DEFAULT (10.0),
    order_total     DOUBLE    NOT NULL,
    order_queue     INTEGER   NOT NULL,
    order_cash      DOUBLE,
    order_change    DOUBLE,
    order_sell_type TEXT (20) NOT NULL
);

INSERT INTO orders (
                       order_id,
                       customer_id,
                       employee_id,
                       store_id,
                       order_price,
                       order_discount,
                       order_total,
                       order_queue,
                       order_cash,
                       order_change,
                       order_sell_type
                   )
                   VALUES (
                       1,
                       1,
                       1,
                       1,
                       200.0,
                       20.0,
                       180.0,
                       1,
                       200.0,
                       20.0,
                       'เสริฟหน้าร้าน'
                   );

INSERT INTO orders (
                       order_id,
                       customer_id,
                       employee_id,
                       store_id,
                       order_price,
                       order_discount,
                       order_total,
                       order_queue,
                       order_cash,
                       order_change,
                       order_sell_type
                   )
                   VALUES (
                       2,
                       2,
                       1,
                       1,
                       350.0,
                       35.0,
                       325.0,
                       2,
                       400.0,
                       75.0,
                       'ทานที่ร้าน'
                   );

INSERT INTO orders (
                       order_id,
                       customer_id,
                       employee_id,
                       store_id,
                       order_price,
                       order_discount,
                       order_total,
                       order_queue,
                       order_cash,
                       order_change,
                       order_sell_type
                   )
                   VALUES (
                       3,
                       3,
                       2,
                       2,
                       200.0,
                       20.0,
                       180.0,
                       1,
                       180.0,
                       0.0,
                       'เสริฟหน้าร้าน'
                   );

INSERT INTO orders (
                       order_id,
                       customer_id,
                       employee_id,
                       store_id,
                       order_price,
                       order_discount,
                       order_total,
                       order_queue,
                       order_cash,
                       order_change,
                       order_sell_type
                   )
                   VALUES (
                       4,
                       4,
                       1,
                       1,
                       350.0,
                       35.0,
                       325.0,
                       3,
                       400.0,
                       75.0,
                       'ทานที่ร้าน'
                   );

INSERT INTO orders (
                       order_id,
                       customer_id,
                       employee_id,
                       store_id,
                       order_price,
                       order_discount,
                       order_total,
                       order_queue,
                       order_cash,
                       order_change,
                       order_sell_type
                   )
                   VALUES (
                       5,
                       5,
                       3,
                       3,
                       250.0,
                       25.0,
                       225.0,
                       1,
                       300.0,
                       75.0,
                       'ทานที่ร้าน'
                   );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name     TEXT (30) UNIQUE,
    product_price    DOUBLE    NOT NULL,
    product_size     TEXT (5)  DEFAULT SML
                               NOT NULL,
    product_type     TEXT (5)  DEFAULT HCF
                               NOT NULL,
    product_sweet_lv TEXT (5)  DEFAULT (123) 
                               NOT NULL,
    product_category           NOT NULL
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_type,
                        product_sweet_lv,
                        product_category
                    )
                    VALUES (
                        1,
                        'โกโก้',
                        40.0,
                        'S, M, L',
                        'HCF',
                        '0123',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_type,
                        product_sweet_lv,
                        product_category
                    )
                    VALUES (
                        2,
                        'ชาเขียว',
                        50.0,
                        'S, M, L',
                        'HCF',
                        '0123',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_type,
                        product_sweet_lv,
                        product_category
                    )
                    VALUES (
                        3,
                        'ชามะลิ',
                        60.0,
                        'S, M, L',
                        'HCF',
                        '0123',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_type,
                        product_sweet_lv,
                        product_category
                    )
                    VALUES (
                        4,
                        'ชาญี่ปุ่นใส',
                        40.0,
                        'S, M, L',
                        'HCF',
                        '0123',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_type,
                        product_sweet_lv,
                        product_category
                    )
                    VALUES (
                        5,
                        'ชาไทย',
                        40.0,
                        'S, M, L',
                        'HCF',
                        '0123',
                        1
                    );


-- Table: stock_check
DROP TABLE IF EXISTS stock_check;

CREATE TABLE stock_check (
    stock_check_id            INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    employee_id               INTEGER   NOT NULL
                                        REFERENCES employee (employee_id),
    store_id                  INTEGER   NOT NULL
                                        REFERENCES store (store_id),
    stock_check_overall_price TEXT (50) NOT NULL
);

INSERT INTO stock_check (
                            stock_check_id,
                            employee_id,
                            store_id,
                            stock_check_overall_price
                        )
                        VALUES (
                            1,
                            1,
                            1,
                            '2400'
                        );

INSERT INTO stock_check (
                            stock_check_id,
                            employee_id,
                            store_id,
                            stock_check_overall_price
                        )
                        VALUES (
                            2,
                            2,
                            2,
                            '700'
                        );


-- Table: store
DROP TABLE IF EXISTS store;

CREATE TABLE store (
    store_id           INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    store_name         TEXT (50),
    store_manager_name TEXT (50) NOT NULL,
    store_address      TEXT (80) NOT NULL,
    store_phone        TEXT (12) NOT NULL
);

INSERT INTO store (
                      store_id,
                      store_name,
                      store_manager_name,
                      store_address,
                      store_phone
                  )
                  VALUES (
                      1,
                      'D-Coffee',
                      'อาจารย์วรวิทย์ วีระพันธุ์',
                      'คณะสหเวชศาสตร์',
                      '02-254-998'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_manager_name,
                      store_address,
                      store_phone
                  )
                  VALUES (
                      2,
                      'D-Coffee',
                      'ผศ.ดร.โกเมศ อัมพวัน',
                      'คณะวิทยาการสารสนเทศ',
                      '02-987-222'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_manager_name,
                      store_address,
                      store_phone
                  )
                  VALUES (
                      3,
                      'D-Coffee',
                      'ดร.พิเชษ วะยะลุน',
                      'คณะพยาบาลศาสตร์',
                      '02-322-123'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_manager_name,
                      store_address,
                      store_phone
                  )
                  VALUES (
                      4,
                      'D-Coffee',
                      'อาจารย์ภูสิต กุลเกษม',
                      'คณะวิทยาศาสตร์',
                      '02-114-789'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_manager_name,
                      store_address,
                      store_phone
                  )
                  VALUES (
                      5,
                      'D-Coffee',
                      'อาจารย์เบญจภรณ์ จันทรกองกุล',
                      'คณะบริหารธุรกิจ',
                      '02-667-055'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_manager_name,
                      store_address,
                      store_phone
                  )
                  VALUES (
                      6,
                      'D-Coffee',
                      'ผศ.จรรยา อ้นปันส์',
                      'คณะแพทยศาสตร์',
                      '02-887-556'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_manager_name,
                      store_address,
                      store_phone
                  )
                  VALUES (
                      7,
                      'D-Coffee',
                      'ผศ.ดร. กฤษณะ ชินสาร',
                      'คณะวิศวกรรมศาสตร์',
                      '02-998-999'
                  );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
