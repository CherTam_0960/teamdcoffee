/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao.test;

import com.mycompany.teamdcoffe.model.OrderItem;
import com.mycompany.teamdcoffe.model.dao.OrderItemDao;

/**
 *
 * @author Nippon
 */
public class testOrderItem {
    public static void main(String[] args) {
        OrderItemDao orderItemDao = new OrderItemDao();

        //Test Run Get Id
        System.out.println(orderItemDao.get(1));
        //Test Run GetAll
        System.out.println(orderItemDao.getAll());
        
        
        //Test Run Save
//        OrderItem orderItem = new OrderItem(6,6,6,1,40.00);
//        orderItemDao.save(orderItem);
//        System.out.println(orderItemDao.getAll());


        //Test Run Update
        OrderItem orderItem = orderItemDao.get(5);
        orderItem.setTotal_price(50.00);
        orderItemDao.update(orderItem);
        System.out.println(orderItemDao.getAll());
        
        //Test Run Delete
//        OrderItem orderItem = orderItemDao.get(6);
//        orderItemDao.delete(orderItem);
//        System.out.println(orderItemDao.getAll());
        
        
//        Test getAll order by
//        orderItemDao.getAll("order_item_total_price", "order_item_total_price ASC");
//        System.out.println(orderItemDao.getAll("order_item_total_price", "order_item_total_price ASC"));
    }
}
