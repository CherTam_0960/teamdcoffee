/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.ui;

import com.mycompany.teamdcoffe.model.Employee;
import com.mycompany.teamdcoffe.model.dao.EmployeeDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class EmployeeService {

    static Employee currentEmp = null;
    public static ArrayList<String> empName = new ArrayList<>();
    public static ArrayList<String> empPass = new ArrayList<>();

    EmployeeDao daoEmp;

    public EmployeeService() {
        daoEmp = new EmployeeDao();
        for (Employee dao : daoEmp.getAll()) {
            empName.add(dao.getName());
        }
    }

    public Employee getCurrentEmp() {
        return currentEmp;
    }

    public void setCurrentEmp(Employee currentEmp) {
        EmployeeService.currentEmp = currentEmp;
    }

    public boolean checkLogin(String empName, String empPass) {
        List<Employee> daou = daoEmp.getAll();

        for (int i = 0; i < daou.size(); i++) {
            if (daou.get(i).getName().equals(empName) && daou.get(i).getName().equals(empPass)) {
                currentEmp = daou.get(i);
                System.out.println(currentEmp);
                return true;
            }
        }
        return false;
    }

    public void clearEmpName() {
        empName.clear();
    }

    public ArrayList<String> getEmpName() {
        return empName;
    }

    public void setEmpName(ArrayList<String> empName) {
        EmployeeService.empName = empName;
    }
}
