/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class Employee {

    private int id;
    private String name;
    private String phone;
    private String address;
    private double salary;
    private String role;

    public Employee(int id, String name, String phone, String address, double salary, String role) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.salary = salary;
        this.role = role;
    }
    
    public Employee(String name, String phone, String address, double salary, String role) {
        this.id = -1;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.salary = salary;
        this.role = role;
    }

    public Employee() {
        this.id=-1;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.salary = salary;
        this.role = role;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", phone=" + phone + ", address=" + address + ", salary=" + salary + ", role=" + role + '}';
    }

       public static Employee fromRS(ResultSet rs) {
           Employee employee = new Employee();
           try {
            employee.setId(rs.getInt("employee_id"));
            employee.setName(rs.getString("employee_name"));
            employee.setPhone(rs.getString("employee_phone"));
            employee.setAddress(rs.getString("employee_address"));
            employee.setSalary(rs.getDouble("employee_salary"));
            employee.setRole(rs.getString("employee_role"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
             return employee;
       }
    
    
    
}
