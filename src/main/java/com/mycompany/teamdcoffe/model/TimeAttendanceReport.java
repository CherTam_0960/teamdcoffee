/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Natthakritta
 */
public class TimeAttendanceReport {

    private int id;
    private String date;
    private String checkIn;
    private String checkOut;
    private int totalTime;
    private int empID;
    private String empName;

    public TimeAttendanceReport(int id, String date, String checkIn, String checkOut, int totalTime, int empID, String empName) {
        this.id = id;
        this.date = date;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalTime = totalTime;
        this.empID = empID;
        this.empName = empName;
    }

    public TimeAttendanceReport(String date, String checkIn, String checkOut, int totalTime, int empID, String empName) {
        this.id = -1;
        this.date = date;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalTime = totalTime;
        this.empID = empID;
        this.empName = empName;
    }
    
    public TimeAttendanceReport(String date, String checkIn, String checkOut, int totalTime, int empID) {
        this.id = -1;
        this.date = date;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalTime = totalTime;
        this.empID = empID;
        this.empName = empName;
    }

    public TimeAttendanceReport() {
        this.id = -1;
        this.date = date;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalTime = totalTime;
        this.empID = empID;
        this.empName = empName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    @Override
    public String toString() {
        return "TimeAttendanceReport{" + "id=" + id + ", date=" + date + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", totalTime=" + totalTime + ", empID=" + empID + ", empName=" + empName + '}';
    }

    public static TimeAttendanceReport fromRS(ResultSet rs) {
        TimeAttendanceReport timeAttendanceReport = new TimeAttendanceReport();
        try {
            timeAttendanceReport.setId(rs.getInt("time_attendance_id"));
            timeAttendanceReport.setDate(rs.getString("time_attendance_date"));
            timeAttendanceReport.setCheckIn(rs.getString("time_attendance_check_in"));
            timeAttendanceReport.setCheckOut(rs.getString("time_attendance_check_out"));
            timeAttendanceReport.setTotalTime(rs.getInt("time_attendance_total_check"));
            timeAttendanceReport.setEmpID(rs.getInt("employee_id"));
            timeAttendanceReport.setEmpName(rs.getString("employee_name"));
        } catch (SQLException ex) {
            Logger.getLogger(TimeAttendanceReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return timeAttendanceReport;
    }
}
