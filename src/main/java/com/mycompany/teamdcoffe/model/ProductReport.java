/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Owen
 */
public class ProductReport {
    
    private int prodId;
    private String prodName;
    private int totalProd;
    private double totalPrice;
    
    public static ProductReport fromRS(ResultSet rs) {
        ProductReport reportProduct = new ProductReport();
        try {
            reportProduct.setProdId(rs.getInt("product_id"));
            reportProduct.setProdName(rs.getString("product_name"));
            reportProduct.setTotalProd(rs.getInt("order_item_total_product"));
            reportProduct.setTotalPrice(rs.getDouble("order_item_total_price"));
        } catch (SQLException ex) {
            Logger.getLogger(ProductReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reportProduct;
    }

    public ProductReport(int prodId, String prodName, int totalProd, int totalPrice) {
        this.prodId = prodId;
        this.prodName = prodName;
        this.totalProd = totalProd;
        this.totalPrice = totalPrice;
    }
    
    public ProductReport(String prodName, int totalProd, int totalPrice) {
        this.prodId = -1;
        this.prodName = prodName;
        this.totalProd = totalProd;
        this.totalPrice = totalPrice;
    }

    public ProductReport() {
        this.prodId = prodId;
        this.prodName = prodName;
        this.totalProd = totalProd;
        this.totalPrice = totalPrice;
    }
    
    
    public int getProdId() {
        return prodId;
    }

    public void setProdId(int prodId) {
        this.prodId = prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public int getTotalProd() {
        return totalProd;
    }

    public void setTotalProd(int totalProd) {
        this.totalProd = totalProd;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ProductReport{" + "prodId=" + prodId + ", prodName=" + prodName + ", totalProd=" + totalProd + ", totalPrice=" + totalPrice + '}';
    }
    
    
    
    
    
}
