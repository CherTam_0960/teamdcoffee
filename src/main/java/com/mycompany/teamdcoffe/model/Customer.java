/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class Customer {

    private int id;
    private String name;
    private String phone;
    private int counttime;
    private String discont;

    public Customer(int id, String name, String phone, int counttime, String discont) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.counttime = counttime;
        this.discont = discont;
    }

    public Customer(String name, String phone, int counttime, String discont) {
        this.id = -1;
        this.name = name;
        this.phone = phone;
        this.counttime = counttime;
        this.discont = discont;
    }

    public Customer() {
        this.id = -1;
        this.name = name;
        this.phone = phone;
        this.counttime = counttime;
        this.discont = discont;
    }

    public Customer(String text, String text0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCounttime() {
        return counttime;
    }

    public void setCounttime(int counttime) {
        this.counttime = counttime;
    }

    public String getDiscont() {
        return discont;
    }

    public void setDiscont(String discont) {
        this.discont = discont;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", phone=" + phone + ", counttime=" + counttime + ", discont=" + discont + '}';
    }
public static Customer fromRS(ResultSet rs) {
           Customer customer = new Customer();
           try {
            customer.setId(rs.getInt("customer_id"));
            customer.setName(rs.getString("customer_name"));
            customer.setPhone(rs.getString("customer_phone"));
            customer.setCounttime(rs.getInt("customer_count_times"));
            customer.setDiscont(rs.getString("customer_discount"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
             return customer;
       }
    
    
    
}