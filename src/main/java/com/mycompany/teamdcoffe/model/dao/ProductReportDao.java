/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.teamdcoffe.model.dao;

import com.mycompany.teamdcoffe.helper.DatabaseHelper;
import com.mycompany.teamdcoffe.model.ProductReport;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Owen
 */
public class ProductReportDao implements Dao<ProductReport> {

    @Override
    public ProductReport get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ProductReport> getAll() {
        ArrayList<ProductReport> list = new ArrayList();
        String sql = "SELECT product.product_id, product.product_name, order_item.order_item_total_product, order_item.order_item_total_price FROM product LEFT JOIN order_item ON product.product_id = order_item.order_item_id ORDER BY product.product_id ASC" ;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductReport item = ProductReport.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ProductReport save(ProductReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ProductReport update(ProductReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(ProductReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ProductReport> getAll(String where, String order) {
        
        ArrayList<ProductReport> list = new ArrayList();
        String sql = "SELECT product.product_id, product.product_name, order_item.order_item_total_product, order_item.order_item_total_price FROM product LEFT JOIN order_item ON product.product_id = order_item.order_item_id ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductReport item = ProductReport.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
