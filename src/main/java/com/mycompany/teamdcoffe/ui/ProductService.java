/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.ui;

import com.mycompany.teamdcoffe.model.Product;
import com.mycompany.teamdcoffe.model.dao.ProductDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nippon
 */
public class ProductService {

    public static List<Product> getAllProduct() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll();
    }

    public static ArrayList<Product> getMockProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list = (ArrayList<Product>) ProductService.getAllProduct();
        return list;
    }
}
