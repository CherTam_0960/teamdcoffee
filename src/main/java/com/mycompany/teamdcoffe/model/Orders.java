/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class Orders {

    private int id;
    private int customer_id;
    private int employee_id;
    private int store_id;
    private double price;
    private double discount;
    private double total;
    private int queue;
    private double cash;
    private double change;
    private String sell_type;
    private ArrayList<OrderItem> orderItems;

    public Orders(int id, int customer_id, int employee_id, int store_id, double price, double discount, double total, int queue, double cash, double change, String sell_type, ArrayList<OrderItem> orderItems) {
        this.id = id;
        this.customer_id = customer_id;
        this.employee_id = employee_id;
        this.store_id = store_id;
        this.price = price;
        this.discount = discount;
        this.total = total;
        this.queue = queue;
        this.cash = cash;
        this.change = change;
        this.sell_type = sell_type;
        this.orderItems = orderItems;
    }

    public Orders(int customer_id, int employee_id, int store_id, double price, double discount, double total, int queue, double cash, double change, String sell_type) {
        this.id = -1;
        this.customer_id = customer_id;
        this.employee_id = employee_id;
        this.store_id = store_id;
        this.price = price;
        this.discount = discount;
        this.total = total;
        this.queue = queue;
        this.cash = cash;
        this.change = change;
        this.sell_type = sell_type;
        this.orderItems = orderItems;
        orderItems = new ArrayList<>();
    }
    

    public Orders() {
        this.id = -1;
        this.customer_id = customer_id;
        this.employee_id = employee_id;
        this.store_id = store_id;
        this.price = price;
        this.discount = discount;
        this.total = total;
        this.queue = queue;
        this.cash = cash;
        this.change = change;
        this.sell_type = sell_type;
        this.orderItems = orderItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public String getSell_type() {
        return sell_type;
    }

    public void setSell_type(String sell_type) {
        this.sell_type = sell_type;
    }

    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", customer_id=" + customer_id + ", employee_id=" + employee_id + ", store_id=" + store_id + ", price=" + price + ", discount=" + discount + ", total=" + total + ", queue=" + queue + ", cash=" + cash + ", change=" + change + ", sell_type=" + sell_type + ", orderItems=" + orderItems + '}';
    }

    public static Orders fromRS(ResultSet rs) {
        Orders order = new Orders();
        try {
            order.setId(rs.getInt("order_id"));
            order.setCustomer_id(rs.getInt("customer_id"));
            order.setEmployee_id(rs.getInt("employee_id"));
            order.setStore_id(rs.getInt("store_id"));
            order.setPrice(rs.getDouble("order_price"));
            order.setDiscount(rs.getDouble("order_discount"));
            order.setTotal(rs.getDouble("order_total"));
            order.setQueue(rs.getInt("order_queue"));
            order.setCash(rs.getDouble("order_cash"));
            order.setChange(rs.getDouble("order_change"));
            order.setSell_type(rs.getString("order_sell_type"));

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return order;
    }
    
//    public void addOrderDetail(OrderItem item){
//        orderItems.add(item);
//        int qty = qty + item.getTotal_product();
//    }
    
//    public void addOrderDetail(Product product, int qty){
//        OrderItem list = new OrderItem();
//        int qtyOrderItem = list.getTotal_product();
//        OrderItem item = new OrderItem(product, this, qtyOrderItem, product.getPrice()*total);
//        this.addOrderDetail(item);
//    }

}
