/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class OrderItem {

    private int id;
    Product product;
    private String name;
    private int product_id;
    private int order_id;
    private int total_product;
    private double total_price;
    private Orders orderItem;
    int index;
    static int count = 1;

    public OrderItem(int id, Product product, int product_id, int order_id, int total_product, double total_price, Orders orderItem) {
        this.id = id;
        this.product = product;
        this.product_id = product_id;
        this.order_id = order_id;
        this.total_product = total_product;
        this.total_price = total_price;
        this.orderItem = orderItem;
    }

    public OrderItem(Product product, int product_id, int order_id, int total_product, double total_price, Orders orderItem) {
        this.id = -1;
        this.product = product;
        this.product_id = product_id;
        this.order_id = order_id;
        this.total_product = total_product;
        this.total_price = total_price;
        this.orderItem = orderItem;
    }
    
        public OrderItem(Product product, Orders orders, int total_product, double total_price) {
        this.id = -1;
        this.product = product;
        this.orderItem = orders;
        this.total_product = total_product;
        this.total_price = total_price;
    }

    public OrderItem(Product product, int total_product, double total_price) {
        this.product = product;
        this.total_product = total_product;
        this.total_price = total_price;
        this.index = count++;
    }

    public OrderItem() {
        this.id = -1;
    }

    public OrderItem(Product product, int total_product) {
        this.product = product;
        this.total_product = total_product;
        this.index = count++;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getTotal_product() {
        return total_product;
    }

    public void setTotal_product(int total_product) {
        this.total_product = total_product;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public Orders getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(Orders orderItem) {
        this.orderItem = orderItem;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        OrderItem.count = count;
    }

    @Override
    public String toString() {
        return "OrderItem{" + "id=" + id + ", product_id=" + product_id + ", order_id=" + order_id + ", total_product=" + total_product + ", total_price=" + total_price + ", orderItem=" + orderItem + '}';
    }

    public static OrderItem fromRS(ResultSet rs) {
        OrderItem orderItem = new OrderItem();
        try {
            orderItem.setId(rs.getInt("order_item_id"));
            orderItem.setProduct_id(rs.getInt("product_id"));
            orderItem.setOrder_id(rs.getInt("order_id"));
            orderItem.setTotal_product(rs.getInt("order_item_total_product"));
            orderItem.setTotal_price(rs.getDouble("order_item_total_price"));

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return orderItem;
    }

}
