/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao.test;

import com.mycompany.teamdcoffe.model.Employee;
import com.mycompany.teamdcoffe.model.dao.EmployeeDao;

/**
 *
 * @author Natthakritta
 */
public class testEmployee {

    public static void main(String[] args) {
        EmployeeDao employeeDao = new EmployeeDao();
        //Test get
        //employee.get(1);
//        System.out.println(employeeDao.get(1));

        //Test getAll
        //employee.getAll();
//        System.out.println(employeeDao.getAll());
        //Test save
//        Employee employee = new Employee("Natthakritta Nawachat", "065-919-0965", "Bamnetnarong Chaiyaphum 36220", 12000, "Counter Staff");
//        employeeDao.save(employee);
//        System.out.println(employeeDao.getAll());
        //Test update
//        Employee employee = employeeDao.get(4);
        //employee.setRole("Counter");
        //employeeDao.update(employee);
        //Test Delete
        //Employee employee = employeeDao.get(4);
        //employeeDao.delete(employee);
        //Test getAll order by
        //employeeDao.getAll("employee_salary", "ASC");
        System.out.println(employeeDao.getAll("employee", "employee_salary ASC"));
        System.out.println(employeeDao.getAll("employee", "employee_name ASC"));
    }
}
