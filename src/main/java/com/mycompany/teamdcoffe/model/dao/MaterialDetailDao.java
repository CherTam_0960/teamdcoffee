/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao;

import com.mycompany.teamdcoffe.helper.DatabaseHelper;
import com.mycompany.teamdcoffe.model.Orders;
import com.mycompany.teamdcoffe.model.Material;
import com.mycompany.teamdcoffe.model.MaterialDetail;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kitti
 */
public class MaterialDetailDao implements Dao<MaterialDetail> {

    @Override
    public MaterialDetail get(int id) {
        MaterialDetail detail = null;
        String sql = "SELECT * FROM material_detail WHERE material_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                detail = MaterialDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return detail;
    }

    @Override
    public List<MaterialDetail> getAll() {
        ArrayList<MaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM material_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialDetail item = MaterialDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<MaterialDetail> getAll(String where, String order) {
        ArrayList<MaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM material_detail where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialDetail item = MaterialDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public MaterialDetail save(MaterialDetail obj) {
        String sql = "INSERT INTO material_detail (stock_check_id,material_id,material_detail_minimum, material_detail_remain, material_detail_overall_price,material_detail_enough)VALUES (?,?,?,?,?,?);";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getStockId());
            stmt.setInt(2, obj.getMaterialId());
            stmt.setInt(3, obj.getMinimum());
            stmt.setInt(4, obj.getRemain());
            stmt.setDouble(5, obj.getOverAllPrice());
            stmt.setBoolean(6, obj.isEnough());

            stmt.executeUpdate();

            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public MaterialDetail update(MaterialDetail obj) {
        String sql = "UPDATE material_detail SET stock_check_id =  ?,material_id = ?,material_detail_minimum =  ?,material_detail_remain =  ?,material_detail_overall_price =  ?,material_detail_enough =  ?WHERE material_detail_id =  ? ;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getStockId());
            stmt.setInt(2, obj.getMaterialId());
            stmt.setInt(3, obj.getMinimum());
            stmt.setInt(4, obj.getRemain());
            stmt.setDouble(5, obj.getOverAllPrice());
            stmt.setBoolean(6, obj.isEnough());
            stmt.setInt(7, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(MaterialDetail obj) {
        String sql = "DELETE FROM material_detail WHERE material_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;   
    }

}
