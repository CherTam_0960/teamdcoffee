/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao;

import com.mycompany.teamdcoffe.helper.DatabaseHelper;
import com.mycompany.teamdcoffe.model.Orders;
import com.mycompany.teamdcoffe.model.Material;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kitti
 */
public class MaterialDao implements Dao<Material> {

    @Override
    public Material get(int id) {
        Material stock = null;
        String sql = "SELECT * FROM material WHERE material_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stock = Material.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stock;
    }

    @Override
    public List<Material> getAll() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material item = Material.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Material> getAllMaterialandDeatail() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT material_id, material_name, material_price_per_unit,material_unit,material_estimate_date,material_detail_minimum FROM material LEFT JOIN  material_detail ON material.material_id = material_detail.material_id";
//        String sql = "SELECT material_id, material_name, material_price_per_unit,material_unit,material_estimate_date FROM material";
//        String sql = "SELECT material_id, material_name, material_price_per_unit,material_unit,material_estimate_date,material_detail_minimum,material_detail_remain FROM material LEFT JOIN  material_detail ON material.material_id = material_detail.material_id;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material item = Material.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Material> getAll(String where, String order) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material item = Material.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Material save(Material obj) {
        String sql = "INSERT INTO material (material_name, material_price_per_unit,material_unit,material_estimate_date) VALUES (?,?,?,?);";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPricePerUnit());
            stmt.setString(3, obj.getUnit());
            stmt.setString(4, obj.getDate());
            stmt.executeUpdate();

            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Material update(Material obj) {
        String sql = "UPDATE material SET material_name = ?,material_price_per_unit = ?,material_unit=?,material_estimate_date = ? WHERE material_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPricePerUnit());
            stmt.setString(3, obj.getUnit());
            stmt.setString(4, obj.getDate());
            stmt.setInt(5, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Material item) {
        String sql = "DELETE FROM material WHERE material_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, item.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
