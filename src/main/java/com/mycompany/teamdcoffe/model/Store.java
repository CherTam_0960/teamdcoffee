/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class Store {

    private int id;
    private String name;
    private String mangerName;
    private String address;
    private String phone;

    public Store(int id, String name, String mangerName, String address,String phone) {
        this.id = id;
        this.name = name;
        this.mangerName = mangerName;
        this.address = address;
        this.phone = phone;
    }

    public Store(String name, String mangerName, String address, String phone) {
        this.id = -1;
        this.name = name;
        this.mangerName = mangerName;
        this.address = address;
        this.phone = phone;
    }

    public Store() {
        this.id = -1;
        this.name = name;
        this.mangerName = mangerName;
        this.address = address;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMangerName() {
        return mangerName;
    }

    public void setMangerName(String mangerName) {
        this.mangerName = mangerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Store{" + "id=" + id + ", name=" + name + ", mangerName=" + mangerName + ", address=" + address + ", phone=" + phone + '}';
    }
    public static Store fromRS(ResultSet rs) {
        Store store = new Store();
        try {
            store.setId(rs.getInt("store_id"));
            store.setName(rs.getString("store_name"));
            store.setMangerName(rs.getString("store_manager_name"));
            store.setAddress(rs.getString("store_address"));
            store.setPhone(rs.getString("store_phone"));
        } catch (SQLException ex) {
            Logger.getLogger(Store.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return store;
    }

}
