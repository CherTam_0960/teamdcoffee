/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class StockReport {

    private int stkId;
    private int empId;
    private String empName;
    private int stkCheckId;
    private int minimum;
    private int remain;

    public static StockReport fromRS(ResultSet rs) {
        StockReport reportStock = new StockReport();
        try {
            reportStock.setStkId(rs.getInt("stock_id"));
            reportStock.setEmpId(rs.getInt("employee_id"));
            reportStock.setEmpName(rs.getString("employee_name"));
            reportStock.setStkCheckId(rs.getInt("stock_check_id"));
            reportStock.setMinimum(rs.getInt("material_detail_minimum"));
            reportStock.setRemain(rs.getInt("material_detail_remain"));
        } catch (SQLException ex) {
            Logger.getLogger(StockReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reportStock;
    }

    public StockReport(int stkId, int empId, String empName, int stkCheckId, int minimum, int remain) {
        this.stkId = stkId;
        this.empId = empId;
        this.empName = empName;
        this.stkCheckId = stkCheckId;
        this.minimum = minimum;
        this.remain = remain;
    }

    public StockReport(int empId, String empName, int stkCheckId, int minimum, int remain) {
        this.stkId = -1;
        this.empId = empId;
        this.empName = empName;
        this.stkCheckId = stkCheckId;
        this.minimum = minimum;
        this.remain = remain;
    }

    public StockReport() {
        this.stkId = stkId;
        this.empId = empId;
        this.empName = empName;
        this.stkCheckId = stkCheckId;
        this.minimum = minimum;
        this.remain = remain;
    }

    public int getStkId() {
        return stkId;
    }

    public void setStkId(int stkId) {
        this.stkId = stkId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public int getStkCheckId() {
        return stkCheckId;
    }

    public void setStkCheckId(int stkCheckId) {
        this.stkCheckId = stkCheckId;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    @Override
    public String toString() {
        return "ReportStock{" + "stkId=" + stkId + ", empId=" + empId + ", empName=" + empName + ", stkCheckId=" + stkCheckId + ", minimum=" + minimum + ", remain=" + remain + '}';
    }

}
