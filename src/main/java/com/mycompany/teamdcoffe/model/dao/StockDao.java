/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao;

import com.mycompany.teamdcoffe.helper.DatabaseHelper;
import com.mycompany.teamdcoffe.model.Orders;
import com.mycompany.teamdcoffe.model.Material;
import com.mycompany.teamdcoffe.model.Stock;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kitti
 */
public class StockDao implements Dao<Stock> {

    @Override
    public Stock get(int id) {
        Stock stock = null;
        String sql = "SELECT * FROM stock WHERE stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stock = Stock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stock;
    }

    @Override
    public List<Stock> getAll() {
        ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM stock";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Stock item = Stock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Stock> getAll(String where, String order) {
        ArrayList<Stock> list = new ArrayList();
//         String sql = "SELECT * FROM employee where " + where + " ORDER BY " + order;
        String sql = "SELECT * FROM stock where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Stock item = Stock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Stock save(Stock obj) {
        String sql = "INSERT INTO stock (employee_id,store_id,stock_check_overall_price ) VALUES (?,?,?);";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setInt(2, obj.getStoreId());
            stmt.setDouble(3, obj.getOverAllPrice());
            stmt.executeUpdate();

            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Stock update(Stock obj) {
        String sql = "UPDATE stock SET employee_id = ? ,store_id = ? ,stock_check_overall_price = ?  WHERE stock_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setInt(2, obj.getStoreId());
            stmt.setDouble(3, obj.getOverAllPrice());
            stmt.setInt(4, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Stock item) {
        String sql = "DELETE FROM stock WHERE stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, item.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
