/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class MaterialDetail {

    private int id;
    private int stockId;
    private int materialId;
    private int minimum;
    private int remain;
    private double overAllPrice;
    private boolean enough;

    public MaterialDetail(int id, int stockId, int materialId, int minimum, int remain, double overAllPrice, boolean enough) {
        this.id = id;
        this.stockId = stockId;
        this.materialId = materialId;
        this.minimum = minimum;
        this.remain = remain;
        this.overAllPrice = overAllPrice;
        this.enough = enough;
    }

    public MaterialDetail(int stockId, int materialId, int minimum, int remain, double overAllPrice, boolean enough) {
        this.id = -1;
        this.stockId = stockId;
        this.materialId = materialId;
        this.minimum = minimum;
        this.remain = remain;
        this.overAllPrice = overAllPrice;
        this.enough = enough;
    }

    public MaterialDetail() {
        this.id = -1;
        this.stockId = stockId;
        this.materialId = materialId;
        this.minimum = minimum;
        this.remain = remain;
        this.overAllPrice = overAllPrice;
        this.enough = enough;
    }

    public double getOverAllPrice() {
        return overAllPrice;
    }

    public void setOverAllPrice(double overAllPrice) {
        this.overAllPrice = overAllPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public boolean isEnough() {
        return enough;
    }

    public void setEnough(boolean enough) {
        this.enough = enough;
    }

    @Override
    public String toString() {
        return "MaterialDetail{" + "id=" + id + ", stockId=" + stockId + ", materialId=" + materialId + ", minimum=" + minimum + ", remain=" + remain + ", overAllPrice=" + overAllPrice + ", enough=" + enough + '}';
    }

    public static MaterialDetail fromRS(ResultSet rs) {
        MaterialDetail materialDetail = new MaterialDetail();
        try {
            materialDetail.setId(rs.getInt("material_detail_id"));
            materialDetail.setStockId(rs.getInt("stock_check_id"));
            materialDetail.setMaterialId(rs.getInt("material_id"));
            materialDetail.setMinimum(rs.getInt("material_detail_minimum"));
            materialDetail.setRemain(rs.getInt("material_detail_remain"));
            materialDetail.setOverAllPrice(rs.getDouble("material_detail_overall_price"));
            materialDetail.setEnough(rs.getBoolean("material_detail_enough"));

        } catch (SQLException ex) {
            Logger.getLogger(MaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return materialDetail;
    }
}
