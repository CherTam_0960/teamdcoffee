/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class Stock {

    private int id;
    private int employeeId;
    private int storeId;
    private double OverAllPrice;

    public Stock(int id, int employeeId, int storeId, double OverAllPrice) {
        this.id = id;
        this.employeeId = employeeId;
        this.storeId = storeId;
        this.OverAllPrice = OverAllPrice;
    }

    public Stock(int employeeId, int storeId, double OverAllPrice) {
        this.id = -1;
        this.employeeId = employeeId;
        this.storeId = storeId;
        this.OverAllPrice = OverAllPrice;
    }

    public Stock() {
        this.id = -1;
        this.employeeId = employeeId;
        this.storeId = storeId;
        this.OverAllPrice = OverAllPrice;
    }

    public static Stock fromRS(ResultSet rs) {
        Stock stock = new Stock();
        try {
            stock.setId(rs.getInt("stock_id"));
            stock.setEmployeeId(rs.getInt("employee_id"));
            stock.setStoreId(rs.getInt("store_id"));
            stock.setOverAllPrice(rs.getInt("stock_check_overall_price"));

        } catch (SQLException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public double getOverAllPrice() {
        return OverAllPrice;
    }

    public void setOverAllPrice(double OverAllPrice) {
        this.OverAllPrice = OverAllPrice;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", employeeId=" + employeeId + ", storeId=" + storeId + ", OverAllPrice=" + OverAllPrice + '}';
    }
}
