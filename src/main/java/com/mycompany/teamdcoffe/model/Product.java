/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kitti
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String size;
    private String type;
    private String sweet_lv;
    private int category;

    public Product(int id, String name, double price, String size, String type, String sweet_lv, int category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.size = size;
        this.type = type;
        this.sweet_lv = sweet_lv;
        this.category = category;
    }
    
    public Product(String name, double price, String size, String type, String sweet_lv, int category) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.size = size;
        this.type = type;
        this.sweet_lv = sweet_lv;
        this.category = category;
    }
    

    public Product() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSweet_lv() {
        return sweet_lv;
    }

    public void setSweet_lv(String sweet_lv) {
        this.sweet_lv = sweet_lv;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", size=" + size + ", type=" + type + ", sweet_lv=" + sweet_lv + ", category=" + category + '}';
    }
    
     public static Product fromRS(ResultSet rs) {
           Product product = new Product();
           try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setPrice(rs.getDouble("product_price"));
            product.setSize(rs.getString("product_size"));
            product.setType(rs.getString("product_type"));
            product.setSweet_lv(rs.getString("product_sweet_lv"));
            product.setCategory(rs.getInt("product_category"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
             return product;
       }
    
    
    
}
