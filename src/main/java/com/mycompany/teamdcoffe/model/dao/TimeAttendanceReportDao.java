/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao;

import com.mycompany.teamdcoffe.helper.DatabaseHelper;
import com.mycompany.teamdcoffe.model.TimeAttendanceReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Natthakritta
 */
public class TimeAttendanceReportDao implements Dao<TimeAttendanceReport> {

    @Override
    public TimeAttendanceReport get(int id) {
        TimeAttendanceReport timeAttendanceReport = null;
        String sql = "SELECT time_attendance.time_attendance_id,time_attendance.time_attendance_id,time_attendance.time_attendance_date,time_attendance.time_attendance_check_in,time_attendance.time_attendance_check_out,time_attendance.time_attendance_total_check,time_attendance.employee_id,employee.employee_name FROM time_attendance LEFT JOIN employee ON employee.employee_id=time_attendance.employee_id WHERE time_attendance.time_attendance_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                timeAttendanceReport = timeAttendanceReport.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return timeAttendanceReport;
    }

    @Override
    public List<TimeAttendanceReport> getAll() {
        ArrayList<TimeAttendanceReport> list = new ArrayList();
        String sql = "SELECT time_attendance.time_attendance_id,time_attendance.time_attendance_id,time_attendance.time_attendance_date,time_attendance.time_attendance_check_in,time_attendance.time_attendance_check_out,time_attendance.time_attendance_total_check,time_attendance.employee_id,employee.employee_name FROM time_attendance LEFT JOIN employee ON employee.employee_id=time_attendance.employee_id";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                TimeAttendanceReport timeAttendance = TimeAttendanceReport.fromRS(rs);
                list.add(timeAttendance);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public TimeAttendanceReport save(TimeAttendanceReport obj) {
        String sql = "INSERT INTO time_attendance (time_attendance_date,time_attendance_check_in,time_attendance_check_out,time_attendance_total_check,employee_id)" + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDate());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalTime());
            stmt.setInt(5, obj.getEmpID());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public TimeAttendanceReport update(TimeAttendanceReport obj) {
        String sql = "UPDATE time_attendance SET time_attendance_date = ?,time_attendance_check_in = ?,time_attendance_check_out = ?,time_attendance_total_check = ?,employee_id =? WHERE time_attendance_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDate());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalTime());
            stmt.setInt(5, obj.getEmpID());
            stmt.setInt(6, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(TimeAttendanceReport obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TimeAttendanceReport> getAll(String where, String order) {
        ArrayList<TimeAttendanceReport> list = new ArrayList();
        String sql = "SELECT time_attendance.time_attendance_id,time_attendance.time_attendance_date,time_attendance.time_attendance_check_in,time_attendance.time_attendance_check_out,time_attendance.time_attendance_total_check,time_attendance.employee_id,employee.employee_name FROM " + where + " LEFT JOIN employee on employee.employee_id=time_attendance.employee_id ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                TimeAttendanceReport timeAttendance = TimeAttendanceReport.fromRS(rs);
                list.add(timeAttendance);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
