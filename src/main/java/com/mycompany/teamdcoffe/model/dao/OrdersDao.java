/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao;

import com.mycompany.teamdcoffe.helper.DatabaseHelper;
import com.mycompany.teamdcoffe.model.OrderItem;
import com.mycompany.teamdcoffe.model.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nippon
 */
public class OrdersDao implements Dao<Orders> {

    @Override
    public Orders get(int id) {
        Orders item = null;
        String sql = "SELECT * FROM orders WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Orders.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Orders> getAll() {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Orders> getAll(String where, String order) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Orders> getAll(String order) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Orders save(Orders obj) {
        String sql = "INSERT INTO orders (customer_id,employee_id,store_id,order_price,order_discount,order_total,order_queue,order_cash,order_change,order_sell_type)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCustomer_id());
            stmt.setInt(2, obj.getEmployee_id());
            stmt.setInt(3, obj.getStore_id());
            stmt.setDouble(4, obj.getPrice());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getTotal());
            stmt.setInt(7, obj.getQueue());
            stmt.setDouble(8, obj.getCash());
            stmt.setDouble(9, obj.getChange());
            stmt.setString(10, obj.getSell_type());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Orders update(Orders obj) {
        String sql = "UPDATE orders SET customer_id =?,employee_id = ?, store_id =?,order_price = ?,order_discount = ?,order_total =?,order_queue = ?,order_cash = ?,order_change =?,order_sell_type = ? WHERE order_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, obj.getCustomer_id());
            stmt.setInt(2, obj.getEmployee_id());
            stmt.setInt(3, obj.getStore_id());
            stmt.setDouble(4, obj.getPrice());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getTotal());
            stmt.setInt(7, obj.getQueue());
            stmt.setDouble(8, obj.getCash());
            stmt.setDouble(9, obj.getChange());
            stmt.setString(10, obj.getSell_type());
            stmt.setInt(11, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;

        }

    }

    @Override
    public int delete(Orders obj) {
        String sql = "DELETE FROM orders WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
