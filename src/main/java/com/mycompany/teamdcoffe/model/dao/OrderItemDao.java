/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teamdcoffe.model.dao;

import com.mycompany.teamdcoffe.helper.DatabaseHelper;
import com.mycompany.teamdcoffe.model.OrderItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nippon
 */
public class OrderItemDao implements Dao<OrderItem> {

    @Override
    public OrderItem get(int id) {
        OrderItem item = null;
        String sql = "SELECT * FROM order_item WHERE order_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = OrderItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<OrderItem> getAll() {
        ArrayList<OrderItem> list = new ArrayList();
        String sql = "SELECT * FROM order_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderItem item = OrderItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<OrderItem> getAll(String where, String order) {
        ArrayList<OrderItem> list = new ArrayList();
        String sql = "SELECT * FROM order_item where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderItem item = OrderItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<OrderItem> getByOrdersId(int orderId) {
        return getAll("order_id = " + orderId, "order_item_id ASC");
    }

    public List<OrderItem> getAll(String order) {
        ArrayList<OrderItem> list = new ArrayList();
        String sql = "SELECT * FROM order_item  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderItem item = OrderItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public OrderItem save(OrderItem obj) {
        String sql = "INSERT INTO order_item (product_id,order_id,order_item_total_product,order_item_total_price)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct_id());
            stmt.setInt(2, obj.getOrder_id());
            stmt.setDouble(3, obj.getTotal_product());
            stmt.setDouble(4, obj.getTotal_price());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public OrderItem update(OrderItem obj) {
        String sql = "UPDATE order_item"
                + " SET product_id = ?, order_id = ?, order_item_total_product = ?, order_item_total_price = ?"
                + " WHERE order_item_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct_id());
            stmt.setInt(2, obj.getOrder_id());
            stmt.setDouble(3, obj.getTotal_product());
            stmt.setDouble(4, obj.getTotal_price());
            stmt.setInt(5, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(OrderItem obj) {
        String sql = "DELETE FROM order_item WHERE order_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
